// VPC
module "test_vpc" {
  source  = "git::https://bitbucket.org/credibilit/terraform-vpc-blueprint.git?ref=0.0.8"
  account = "${var.account}"

  name                      = "cloudwatch_test_elb"
  domain_name               = "cloudwatch_test_elb.local"
  cidr_block                = "10.0.0.0/16"
  azs                       = "${data.aws_availability_zones.azs.names}"
  az_count                  = 4
  hosted_zone_comment       = "An internal hosted zone for testing"
  public_subnets_cidr_block = [
    "10.0.0.0/24",
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24"
  ]
}

resource "aws_instance" "web" {
  ami                     = "${data.aws_ami.ubuntu.id}"
  instance_type           = "t2.micro"
  subnet_id               = "${element(module.test_vpc.public_subnets, 0)}"
  vpc_security_group_ids  = [
    "${aws_security_group.instance_http.id}"
  ]
  tags {
    Name    = "TestCloudWatchELB"
    CSC_MON = ""
    CSC_SEG = ""
  }
  count = 2
}

resource "aws_elb_attachment" "web" {
  elb      = "${module.elb_test.id}"
  instance = "${element(aws_instance.web.*.id, count.index)}"

  count = 2
}

module "elb_test" {
  source  = "git::https://bitbucket.org/credibilit/terraform-https-elb-blueprint.git?ref=0.0.2"
  account = "${var.account}"

  name                    = "TestCloudWatchELB"
  instance_security_group = "${aws_security_group.instance_http.id}"
  subnets                 = ["${module.test_vpc.public_subnets}"]
  s3_bucket_force_destroy = true
  internal                = false
  tags                    = "${map("Environment", "test")}"
  ssl_certificate_arn     = "${aws_iam_server_certificate.acme.arn}"
}

resource "aws_iam_server_certificate" "acme" {
  name             = "TestCloudWatchELB"
  certificate_body = "${file("keys/acme.com.crt")}"
  private_key      = "${file("keys/acme.com.key")}"
}

resource "aws_security_group" "instance_http" {
  name        = "test-cloud-watch-elb-instance"
  description = "Allow traffic from ELB"
  vpc_id      = "${module.test_vpc.vpc}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


// Action
resource "aws_sns_topic" "monitoring" {
  name         = "monitoring"
  display_name = "monitoring"
}

module "test_elb_cloudwatch_web" {
  source  = "../"
  account = "${var.account}"

  account_name  = "credibilit-tst"
  env           = "tst"
  elb_id        = "${module.elb_test.id}"
  elb_name      = "TestCloudWatchELB"
  alarm_actions = ["${aws_sns_topic.monitoring.arn}"]
}
