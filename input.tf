variable "account" {
  description = "The AWS account number"
}

variable "account_name" {
  description = "The AWS account name or alias"
}

variable "env" {
  description = "The environment name"
}

variable "elb_id" {
  description = "The AWS ec2 instance id"
}

variable "elb_name" {
  description = "The AWS ec2 instance name"
}

variable "alarm_actions" {
  type = "list"
  description = "The AWS Cloud Watch alarm actions"
}

// Unhealthy Host Count
variable "unhealthy_host_count_comparison_operator" {
  default = "GreaterThanThreshold"
}

variable "unhealthy_host_count_evaluation_periods" {
  default = 3
}

variable "unhealthy_host_count_period" {
  default = 300
}

variable "unhealthy_host_count_statistic" {
  default = "Average"
}

variable "unhealthy_host_count_threshold" {
  default = 0.9
}

variable "unhealthy_host_count_unit" {
  default = ""
}
