# Unhealthy Host Count
resource "aws_cloudwatch_metric_alarm" "unhealthy_host_count" {
  alarm_name          = "[${var.account_name}] [elb] [${var.env}] ${var.elb_name} - Unhealthy Host Count"
  comparison_operator = "${var.unhealthy_host_count_comparison_operator}"
  evaluation_periods  = "${var.unhealthy_host_count_evaluation_periods}"
  metric_name         = "UnHealthyHostCount"
  namespace           = "AWS/ELB"
  period              = "${var.unhealthy_host_count_period}"
  statistic           = "${var.unhealthy_host_count_statistic}"
  threshold           = "${var.unhealthy_host_count_threshold}"
  unit                = "${var.unhealthy_host_count_unit}"
  alarm_actions       = ["${var.alarm_actions}"]
  dimensions {
    LoadBalancerName = "${var.elb_name}"
  }
}

output "unhealthy_host_count" {
  value = {
    id = "${aws_cloudwatch_metric_alarm.unhealthy_host_count.id}"
  }
}
